# Project Overview

## Technology overview

This is the in used technology stack

- [Python](https://www.python.org/)
- [React](https://reactjs.org)
- [Docker](http://docker.com)
- [Visual Sudio Code](https://code.visualstudio.com/)
- [Git](http://git.com)
- [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/contents/)
- [Cypress](https://cypress.io)

## Visual Studio Code Configuration

The following Visual Studio Code Extensions are used:

- Git Extenstion Pack
- Markdown PDF
- AutoClose Tags
- Aute Rename Tags
- Bracket Pair Colorizer 2
- Compare It
- Pylance (Python IntelliSense)
- Python

## Git

[Main Branch](https://gitlab.com/benjamin-gabriel/foodstar/-/tree/main)

Git Commit Types:

Syntax convention to commit any progress:

- type(Added): commit message
- type(Changed): commit message
- type(Fixed): commit message

### Delete a branch

- (local)
  - git branch
  - git branch -d _branch_
- (remote)
  - git branch -a
  - git push origin -d _branch_

# Basic Configuration

## Visual Stuido Debug mode

Remote Attach Debugpy

- Request = attach
- Docker Compose Build Target = debug
- Test: <http://localhost:8000>

## Webservice Urls

### Test Urls

<http://localhost:8000> (check connection)
<http://localhost:8000/api/v1/productlist?name=Snickers> (Search result)
<http://localhost:8000/api/v1/productinfo?id=5000159474375> (product)
<http://localhost:8000/api/v1/productcount> (number of products in DB)

# Start Application

### Virtual environment for Python

Python Virtual environemnt maintains python packages at a central local path.

Commands are used from: `./backend/.venv/Scripts`

Additionally for code linting it is useful to include an virtual environment independant of any docker processes. The local virtual environment can be configured in VS-Code as follows:

- `py -m venv ./backend`
- `source ./backend/.venv/scripts/activate`
- Alternative in VS-Code: Select environemnt: Ctrl+Shift+P --> select interpreter --> python venv

## Pyhton linter and formatter

`See .vscode/settings.json`

- flake8 (linter)
- black (auto formatter)

# Frontend

The Frontend webview is implemented in React. The path is:

./frontend/react-app

NPM / NPX packages are used globally. See:

- npm list -g --depth=0 (list all packages)

## TypeScript

- npm install typescript --save-dev
- npm install webpack webpack-cli ts-loader

- add files
  **tsconfig.js
  **webpack.config.js

- npm install react react-dom @types/react @types/react-dom

# Backend

The backend is implemented in python and communicates via webserver "flask requests" forwarding to the FE.

The data infrastructure is managed in the repository namespace (see: ./backend/src/repository)

## Python packages

Ckeck all used packages:

`pip list`

Packes are available in:

`backend\.venv\Lib`

## Mongo DB

Mongo DB and Mongo DB express are defined in ./infrastructure/stack.yml as containers.

Mongo DB expres: http://localhost:8080

Mongo DB http://localhost:27016

# Infrastructure

## Docker

The following docker packages are used:

| Dockerfile           | Build Image             | Delete Image             | Size    | Run Service                                            | Delete Service       |
| -------------------- | ----------------------- | ------------------------ | ------- | ------------------------------------------------------ | -------------------- |
| ./backend/dockerfile | docker build -t pyweb . | docker rmi --force pyweb | 1.19 GB | docker run -d -p 8000:5000 --name pywebcontainer pyweb | docker rm _instance_ |

See all running images

- docker ps --all

See all expired container

- docker ps -f "status=exited"

Delete stoped container

- docker rm _container_id_

All running containers
docker ps -f "status=running"

Further documentation:
https://gitlab.com/benjamin-gabriel/istqb-practise/-/wikis/Home/Docker

## Docker-Compose

# Test and Deploy

This chapter summarizes the test automation appraoches for backend and frontend.

## Unit Test Execution

The following libraries are used for unit testing:

- unittest2
- from unittest.mock import patch, Mock

documentation: <https://docs.python.org/dev/library/unittest.html>

Run the unit tests:

- `python -m unittest2`

Generate reports:

- `py -m xmlrunner discover -o ../docs/testreports`

## Integration Testing

### Cypress

#### Installation

- installation tasks
  ** location: frontend/react-app
  ** install: npm i --dev cypress @testing-library/cypress
  ** adapt commands: supprt/commands.js --> import "@testing-library/cypress/add-commands";
  ** create config: npx cypress open
- install chrome extension `Testing Playground`

#### Execute

- npx cypress run
- run only one test: it.only(...)

# Project Content

## Name

Foodstar - quick food evaluation, see differnces.

## Description

This project gives an overview of common values of food, visualize and compare facts.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Support

benjamin.gabriel@outlook.de

## Roadmap

- Compare food facts
- Create an account and save favorites

## Contributing

Data and inspiration by [Open Food Facts](https://world.openfoodfacts.org/)

## License

GNU General Public License

## Project status

work in progress

# Backup

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
