import React, { Component } from "react";
import "./App.css";
import "./components/header";
import "./components/mainarea";
import MainArea from "./components/mainarea";
import Header from "./components/header";
import axios from "axios";
import "./components/detailedPage";
import DetailedPage from "./components/detailedPage";
import * as DataSourceMock from "./assets/testproductdata";
import * as Globals from "./components/globals";
import queryString from "query-string";
import result from "./components/result";

const PageContext = React.createContext();
class App extends Component {
  state = {
    dataSourceType: Globals.DATASOURCE_WEBREQUEST,
    pageSize: 0,
    currentPage: 0,
    numberOfAllElements: 0,
    loadedResults: [],
    apiUrl: "",
    detailedPage: {
      productId: 0,
      isModal: false,
      productData: null,
    },
  };

  constructor() {
    super();
    document.addEventListener("DOMContentLoaded", this.pageLoadedHandler);
    this.state.apiUrl =
      process.env.REACT_APP_PROTOCOL +
      "://" +
      process.env.REACT_APP_HOST +
      ":" +
      process.env.REACT_APP_PORT +
      process.env.REACT_APP_API_VERSION;

    this.state.pageSize = process.env.REACT_APP_PAGE_SIZE;

    const queryParams = queryString.parse(window.location.search);
    if (queryParams.data === Globals.DATASOURCE_MOCK) {
      this.state.dataSourceType = Globals.DATASOURCE_MOCK;
    }
  }

  pageHandler = (pageId) => {
    const numberOfPages = Math.ceil(
      parseInt(this.state.loadedResults.length) / parseInt(this.state.pageSize)
    );
    if (pageId > 0 && pageId <= numberOfPages) {
      this.setState({ currentPage: pageId });
    }
  };

  pageLoadedHandler = async () => {
    const url = this.state.apiUrl + "/productcount";

    const result = await this.getSearchResultData(
      url,
      Globals.PRODUCT_COUNT,
      DataSourceMock.TEST_FILE_MOCK_COUNT
    );

    this.setState({ numberOfAllElements: result });
  };

  detailPageHandler = async (productId) => {
    const url = this.state.apiUrl + "/productinfo?id=" + productId;

    const result = await this.getSearchResultData(
      url,
      "",
      DataSourceMock.TEST_FILE_MOCK_PRODUCT
    );
    this.setState({
      detailedPage: {
        ...this.state.detailedPage,
        isModal: true,
        productId: productId,
        productData: result,
      },
    });
  };

  componentDidUpdate(prevProps, prevState) {
    prevState.detailedPage.productData = null;
  }

  searchHandler = async (searchTerm, e) => {
    if (e !== undefined && e.type === "keydown" && e.code !== "Enter") {
      return;
    }

    const url = this.state.apiUrl + "/productlist?name=" + searchTerm;

    const results = await this.getSearchResultData(
      url,
      Globals.PORDUCT_OVERVIEW_ITEM,
      DataSourceMock.TEST_FILE_MOCK_SEARCH_RESULT
    );

    this.setState({
      loadedResults: results,
      currentPage: 1,
    });
  };

  async getSearchResultData(url, key, datasource) {
    let loadedResults = "";
    if (this.state.dataSourceType === Globals.DATASOURCE_WEBREQUEST) {
      await axios
        .get(url)
        .then((resp) => {
          const productObj = JSON.parse(resp.data);
          loadedResults = key !== "" ? productObj[key] : productObj;
        })
        .catch((error) => {
          console.log(error.message);
        });
    } else if (this.state.dataSourceType === Globals.DATASOURCE_MOCK) {
      const productObj = JSON.parse(datasource);
      loadedResults = key !== "" ? productObj[key] : productObj;
    }
    return loadedResults;
  }

  render() {
    return (
      <React.Fragment>
        <Header onSearchAction={this.searchHandler} />
        <PageContext.Provider value={this.state.currentPage}>
          <MainArea
            numberOfAllElements={this.state.numberOfAllElements}
            pageSize={this.state.pageSize}
            results={this.state.loadedResults}
            onPageChange={this.pageHandler}
            onDetailedPage={this.detailPageHandler}
          ></MainArea>
          <DetailedPage
            detailedPage={this.state.detailedPage}
            onDetailedPage={this.detailPageHandler}
          />
        </PageContext.Provider>
      </React.Fragment>
    );
  }
}
export { PageContext };
export default App;
