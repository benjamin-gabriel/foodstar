import DetailedPage from "../components/detailedPage";
import { render, screen } from "@testing-library/react";

let emptyHandler = undefined;

beforeAll(() => {
  emptyHandler = () => void 0;
});

test("GIVEN no product data, WHEN is loaded, THEN container tag is empty", () => {
  // GIVEN
  const initProductData = {
    productId: 0,
    isModal: false,
    productData: null,
  };

  // WHEN
  const { container } = render(
    <DetailedPage
      detailedPage={initProductData}
      onDetailedPage={emptyHandler}
    />
  );

  // THEN
  expect(container.firstChild).toBeEmptyDOMElement();
});

test("GIVEN detailed page, WHEN is loaded, THEN all attributes are in DOM", () => {
  // given
  const productdata =
    '{"product_id": "5000159474375", "product_name": "Snickers Minis 15er", "nutritional_value": {"energy": 2017, "energy_kcal": "484", "fat": 22.5, "saturated_fat": 7.9, "carbs": 60.5, "sugar": 51.8, "protein": 8.6, "salt": 0.63}, "image_url": "https://world.openfoodfacts.org/images/products/500/015/947/4375/front_de.3.200.jpg"}';

  // const productObj = JSON.parse(datasource);
  const productdataParsed = JSON.parse(productdata);

  const initProductData = {
    productId: 5000159474375,
    isModal: true,
    productData: productdataParsed,
  };

  const numberOfEntryRows = 8;

  // when
  render(
    <DetailedPage
      detailedPage={initProductData}
      onDetailedPage={emptyHandler}
    />
  );

  const htmlTableElement = screen.getByTestId("nutriTable");

  // then
  // table --> tbody --> tr
  expect(htmlTableElement.children[1].children.length).toBeGreaterThanOrEqual(
    8
  );
  expect(
    htmlTableElement.children[1].children[0].children[0].innerHTML
  ).toContain("energy");
  expect(
    String(htmlTableElement.children[1].children[0].children[1].innerHTML)
  ).toContain("2017");
});
