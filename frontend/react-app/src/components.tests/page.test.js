import Page from "../components/Page";
import { render, screen } from "@testing-library/react";
import { TEST_FILE_MOCK_COUNT } from "../assets/testproductdata";

test("page link is appeard correctly", () => {
  const emptyFunction = () => void 0;
  const pageNumber = 2;

  render(
    <Page
      key={1}
      page={{ pageId: pageNumber, isCurrentPage: false }}
      onPageChange={emptyFunction}
    />
  );
  // screen.debug();

  const pageLink = screen.getByText(pageNumber);
  expect(pageLink).toHaveAttribute("href", "#");
  expect(pageLink.innerHTML.trim()).toEqual(expect.stringMatching(/2/i));
});
