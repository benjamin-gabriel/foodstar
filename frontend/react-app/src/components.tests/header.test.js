import Header from "../components/Header";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

const { faker } = require("@faker-js/faker");

describe("Header elements", () => {
  describe("Logo", () => {
    it('then logo needs to have src = "/logo.png" and alt = "Foodstar Logo"', () => {
      render(<Header />);
      const logo = screen.getByRole("img");
      expect(logo).toHaveAttribute("src", "logo.png");
      expect(logo).toHaveAttribute("alt", "Foodstar Logo");
    });
  });
  describe("Input Field", () => {
    it("then input-field in document", () => {
      render(<Header />);
      const inputText = document.getElementById("input-data");
      expect(inputText).toBeInTheDocument();
    });
  });
  describe("Search Button", () => {
    it("then search button in document", () => {
      render(<Header />);
      const button = screen.getByText(/SUCHEN/i);
      expect(button).toBeInTheDocument();
    });
  });
});

describe("Header elements behaviour", () => {
  test("GIVEN input field with disabled button, WHEN enter text into input field, THEN button switched to enabled", async () => {
    // given
    const emptyHandler = () => void 0;
    render(<Header onSearchAction={emptyHandler} />);
    const searchTerm = faker.commerce.product();
    expect(screen.getByText(/suchen/i).closest("button")).toBeDisabled();

    // when
    userEvent.type(screen.getByPlaceholderText(/Eingabe/i), searchTerm);

    // then
    expect(await screen.getByText(/suchen/i).closest("button")).toBeEnabled();
  });
});
