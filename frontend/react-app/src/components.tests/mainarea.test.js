import PageNav from "../components/mainarea";
import { render, screen } from "@testing-library/react";
import TestRenderer from "react-test-renderer";
import MainArea from "../components/mainarea";

test("given 3 results found \
     when check status text \
     then 3 is appearing in text", () => {
  const emptyHandler = () => void 0;
  const loadedResults = [1, 2, 3];

  const testRenderer = TestRenderer.create(
    <MainArea
      numberOfAllElements={1212}
      pageSize={5}
      results={loadedResults}
      onPageChange={emptyHandler}
      onDetailedPage={emptyHandler}
    />
  );

  const testInstance = testRenderer.root;

  const statusTag = testInstance.findByProps({
    className: "row justify-content-center",
  });

  expect(String(statusTag.children)).toEqual(
    expect.stringMatching(/3 Produkte gefunden/i)
  );
});
