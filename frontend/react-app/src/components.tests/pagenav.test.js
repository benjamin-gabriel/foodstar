import PageNav from "../components/pagenav";
import { render, screen } from "@testing-library/react";
import TestRenderer from "react-test-renderer";

test("page navigation has paging buttons", () => {
  const onPageChangeEmptyFunction = () => void 0;

  const pagingValue = {
    numberOfResults: 13,
    pageSize: 5,
  };

  const pageNumber = 2;

  render(
    <PageNav
      paging={pagingValue}
      currentPage={pageNumber}
      onPageChange={onPageChangeEmptyFunction}
    />
  );
  //screen.debug();

  const buttonLeft = screen.getByTestId("buttonLeft");
  const buttonRight = screen.getByTestId("buttonRight");
  expect(buttonLeft.type).toBe("button");
  expect(buttonRight.type).toBe("button");
});

test("given search, when result count < pagesize, then page link count is 1", () => {
  // given
  const onPageChangeEmptyFunction = () => void 0;
  const pagingValue = {
    numberOfResults: 4,
    pageSize: 5,
  };
  const pageNumber = 1;

  // when
  const testRenderer = TestRenderer.create(
    <PageNav
      paging={pagingValue}
      currentPage={pageNumber}
      onPageChange={onPageChangeEmptyFunction}
    />
  );
  const testInstance = testRenderer.root;

  // then
  expect(
    testInstance.findByProps({ className: "col-sm-2 p-0 offset-0 text-center" })
      .children.length
  ).toBe(1);
});
