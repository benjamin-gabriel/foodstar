import Results from "./results";
import PageNav from "./pagenav";
import { PageContext } from "../App";
import React, { useContext } from "react";

function MainArea(props) {
  const currentPage = useContext(PageContext);
  const pagingValue = {
    numberOfResults: props.results.length,
    pageSize: props.pageSize,
  };

  //const [pageResults, setPageResults] = useState([]);
  const statusText = getStatusTextByCurrentPage(currentPage);

  return (
    <React.Fragment>
      <main className={"container"}>
        <div className={"row justify-content-center"}>{statusText}</div>
        <Results
          results={props.results}
          currentPage={currentPage}
          pageSize={props.pageSize}
          onDetailedPage={props.onDetailedPage}
        />
        <PageNav
          paging={pagingValue}
          currentPage={currentPage}
          onPageChange={props.onPageChange}
        />
      </main>
    </React.Fragment>
  );

  function getStatusTextByCurrentPage(currentPage) {
    let statusText = "";
    statusText =
      currentPage === 0 && props.results.length === 0
        ? props.numberOfAllElements + " Produkte verfügbar."
        : props.results.length === 0
        ? "Keine Produkte gefunden"
        : props.results.length + " Produkte gefunden";
    return statusText;
  }
}

export default MainArea;
