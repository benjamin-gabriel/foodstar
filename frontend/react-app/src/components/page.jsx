import React from "react";
import "./detailedPage";

function Page(props) {
  if (props.page.isCurrentPage !== true) {
    let link = (
      <a onClick={() => props.onPageChange(props.page.pageId)} href={"#"}>
        {props.page.pageId}
      </a>
    );
    return <span> {link} </span>;
  }
  return <span> {props.page.pageId} </span>;
}
export default Page;
