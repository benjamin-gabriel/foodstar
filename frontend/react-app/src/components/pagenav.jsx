import React, { Component } from "react";
import Page from "./page";
import * as Globals from "./globals";

function changeBackground(e) {
  console.log(e);
  e.target.style.background = "red";
}
class PageNav extends Component {
  state = {
    pages: [],
    currentPageIndex: 0,
  };

  currentPageHandler(identifier) {
    if (
      identifier === "next" &&
      this.state.currentPageIndex < this.state.numberOfPages
    ) {
      this.state.currentPageIndex++;
      // this.setState((prevState) => ({
      //   currentPageIndex: prevState.state.currentPageIndex + 1,
      // }));
    }
  }

  generatePaging(numberOfResults, pageSize) {
    let numberOfPages = Math.ceil(numberOfResults / pageSize);
    if (pageSize <= 0) {
      return [];
    }
    let pageLinks = [];

    for (let pageIndex = 1; pageIndex <= numberOfPages; pageIndex++) {
      let pageLink = {
        pageId: pageIndex,
        isCurrentPage: pageIndex === this.state.currentPageIndex,
      };
      pageLinks.push(pageLink);
    }

    return pageLinks;
  }

  validateHideElementStyle(currentPage) {
    if (currentPage === 0) return "d-none";
    else return "";
  }

  render() {
    this.state.currentPageIndex = this.props.currentPage;
    //this.setState({ currentPageIndex: this.props.currentPage });

    this.state.pages = this.generatePaging(
      this.props.paging.numberOfResults,
      this.props.paging.pageSize
    );

    return (
      <div className={"row justify-content-center align-items-center p-2"}>
        <div style={{}} className={"col-sm-1 p-0 offset-0 text-right"}>
          <button
            type="button"
            data-testid="buttonLeft"
            onClick={() =>
              this.props.onPageChange(this.state.currentPageIndex - 1)
            }
            id={"buttonPagingRight"}
            style={{
              fontSize: "0.7rem",
              color: "white",
              backgroundColor: "green",
            }}
            className={`btn btn-primary bi bi-box-arrow-left ${this.validateHideElementStyle(
              this.state.currentPageIndex
            )}`}
            onMouseOver={Globals.handleMouseEnter}
            onMouseOut={Globals.handleMouseLeave}
          ></button>
        </div>
        <div style={{}} className={"col-sm-2 p-0 offset-0 text-center"}>
          {this.state.pages.map((page) => (
            <Page
              key={this.state.pages.indexOf(page)}
              page={page}
              onPageChange={this.props.onPageChange}
            />
          ))}
        </div>
        <div className={"col-sm-1 p-0 offset-0 text-left"}>
          <button
            type="button"
            data-testid="buttonRight"
            onClick={() =>
              this.props.onPageChange(this.state.currentPageIndex + 1)
            }
            id={"buttonPagingRight"}
            style={{
              fontSize: "0.7rem",
              color: "white",
              backgroundColor: "green",
            }}
            onMouseOver={Globals.handleMouseEnter}
            onMouseOut={Globals.handleMouseLeave}
            className={`btn btn-primary bi bi-box-arrow-right ${this.validateHideElementStyle(
              this.state.currentPageIndex
            )}`}
          ></button>
        </div>
      </div>
    );
  }
}
export default PageNav;
