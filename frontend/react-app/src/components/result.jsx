import React, { Component } from "react";
import defaultImage from "../assets/default_food.png";
import PageContext from "../App";
import DetailedPage from "./detailedPage";
import * as Globals from "./globals";

class result extends Component {
  state = {
    detailUrl:
      window.location.protocol +
      window.location.hostname +
      "/?id=" +
      this.props.result[Globals.PRODUCT_OVERVIEW_ID],
  };

  render() {
    //   const linkElement = (
    //     src: "",
    //     onClick: {() => this.props.onDetailedPage(this.props.result[Globals.PRODUCT_OVERVIEW_ID])},
    //     className: {"a-overview bi bi-box-arrow-in-up-right"}
    // );
    const linkAttributes = {
      href: "#",
      onClick: () =>
        this.props.onDetailedPage(
          this.props.result[Globals.PRODUCT_OVERVIEW_ID]
        ),
    };

    const imageAttributes = {
      style: { width: "100px" },
      src:
        this.props.result[Globals.PRODUCT_OVERVIEW_THUMBNAIL_URL] !== ""
          ? this.props.result[Globals.PRODUCT_OVERVIEW_THUMBNAIL_URL]
          : defaultImage,
      alt: "food picture",
    };

    return (
      <div
        className={("row", "row justify-content-start align-items-center p-2")}
      >
        <div style={{ textAlign: "right" }} className={"col-sm-2 p-0 offset-1"}>
          <a {...linkAttributes}>
            <img {...imageAttributes}></img>
          </a>
        </div>
        <div className={"col-sm-6"}>
          <div className={"row p-1"}>
            <a
              {...linkAttributes}
              className={"a-overview bi bi-box-arrow-in-up-right"}
            >
              {this.props.result[Globals.PRODUCT_OVERVIEW_NAME]}
            </a>{" "}
          </div>
          <div className={"row p-2"}>
            {this.props.result[Globals.PRODUCT_OVERVIEW_DESCRIPTION]}
          </div>
        </div>
      </div>
    );
  }
}

export default result;
