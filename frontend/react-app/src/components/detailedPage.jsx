import React, { Component, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import * as Globals from "./globals";

function DetailedPage(props) {
  const [show, setShow] = useState(false);
  const [isClosed, setClose] = useState(false);

  const handleClose = () => {
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };

  React.useEffect(() => {
    setShow(props.detailedPage.isModal);
  }, [props]);

  if (
    props.detailedPage.productData === null ||
    props.detailedPage.productData === undefined
  ) {
    return <div></div>;
  } else {
    const nutritions =
      props.detailedPage.productData[Globals.PRODUCT_DETAIL_NUTRITIONAL_VALUE];
    return (
      <>
        {/* {
          <Button variant="primary" onClick={handleShow}>
            Launch demo modal
          </Button>
        } */}

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>
              {props.detailedPage.productData[Globals.PRODUCT_DETAIL_NAME]}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <img
                src={
                  props.detailedPage.productData[
                    Globals.PRODUCT_DETAIL_IMAGE_URL
                  ]
                }
              ></img>
            </div>
            <div>
              <table
                data-testid="nutriTable"
                className="table table-striped table-bordered"
              >
                <thead>
                  <tr>
                    <th colSpan="2">Nährwerte (100g)</th>
                  </tr>
                </thead>
                <tbody>
                  {Object.entries(nutritions).map(([key, value], index) => (
                    <tr key={index}>
                      <td>{key}</td>
                      <td>{value}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              onClick={() => {
                handleClose();
              }}
              className={`btn btn-primary bi bi-x-square
              )}`}
              style={{
                fontSize: "0.7rem",
                color: "white",
                backgroundColor: "green",
              }}
              onMouseOver={Globals.handleMouseEnter}
              onMouseOut={Globals.handleMouseLeave}
            >
              {" "}
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default DetailedPage;
