export const PRODUCT_COUNT = "product_count";

// product overview keys
export const PORDUCT_OVERVIEW_ITEM = "food_overview_item";
export const PRODUCT_OVERVIEW_ID = "product_id";
export const PRODUCT_OVERVIEW_NAME = "product_name";
export const PRODUCT_OVERVIEW_THUMBNAIL_URL = "thumbnail_url";
export const PRODUCT_OVERVIEW_DESCRIPTION = "product_description";

// product detail keys

export const PRODUCT_DETAIL_ID = "product_id";
export const PRODUCT_DETAIL_NAME = "product_name";
export const PRODUCT_DETAIL_NUTRITIONAL_VALUE = "nutritional_value";
export const PRODUCT_DETAIL_NUTRITIONAL_ENERGY = "energy";
export const PRODUCT_DETAIL_NUTRITIONAL_ENERGY_KCAL = "energy_kcal";
export const PRODUCT_DETAIL_NUTRITIONAL_FAT = "fat";
export const PRODUCT_DETAIL_NUTRITIONAL_SATURATED_FAT = "saturated_fat";
export const PRODUCT_DETAIL_NUTRITIONAL_CARBS = "carbs";
export const PRODUCT_DETAIL_NUTRITIONAL_SUGAR = "sugar";
export const PRODUCT_DETAIL_NUTRITIONAL_PROTEIN = "protein";
export const PRODUCT_DETAIL_NUTRITIONAL_SALT = "salt";
export const PRODUCT_DETAIL_IMAGE_URL = "image_url";

// button colors
export const handleMouseEnter = (e) => {
  e.target.style.background = "black";
};
export const handleMouseLeave = (e) => {
  e.target.style.background = "green";
};

export const DATASOURCE_MOCK = "mock";
export const DATASOURCE_WEBREQUEST = "webrequest";
