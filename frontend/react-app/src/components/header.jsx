import React, { Component } from "react";
import logo from "../assets/logo.png";
import * as Globals from "./globals";

class Header extends Component {
  state = {
    logoWidth: "350px",
    searchTerm: "",
    isButtonDisabled: true,
  };

  render() {
    return (
      <header>
        <div className="container p-5" id="input">
          <div className="row justify-content-center p-5">
            <img
              className="header"
              src={logo}
              style={{ width: this.state.logoWidth }}
              alt="Foodstar Logo"
            ></img>
          </div>
          <div className="row justify-content-center" id="document-info">
            <p></p>
          </div>
          <div className="row justify-content-center" id="input">
            <div className="col-sm-7 justify-content-end p-0">
              <input
                onChange={(e) => {
                  this.setState({
                    searchTerm: e.target.value,
                    isButtonDisabled: e.target.value !== "" ? false : true,
                  });
                }}
                onKeyDown={(e) =>
                  this.props.onSearchAction(this.state.searchTerm, e)
                }
                type="text"
                placeholder="Eingabe"
                name="producttoken"
                id="input-data"
                className="form-control form-control-lg"
                style={{ color: "green", fontSize: "1rem" }}
              ></input>
            </div>
            <div className="col-sm-2 justify-content-start p-0">
              <button
                onClick={() => this.props.onSearchAction(this.state.searchTerm)}
                id="search-button"
                className="btn btn-primary bi bi-search"
                style={{
                  fontSize: "1rem",
                  color: "white",
                  backgroundColor: "green",
                }}
                onMouseOver={Globals.handleMouseEnter}
                onMouseOut={Globals.handleMouseLeave}
                disabled={this.state.isButtonDisabled}
              >
                Suchen
              </button>
            </div>
          </div>
        </div>
      </header>
    );
  }
}
export default Header;
