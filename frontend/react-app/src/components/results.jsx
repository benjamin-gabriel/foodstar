import React, { Component } from "react";
import Result from "./result";

class Results extends Component {
  state = {
    pageResults: [],
  };

  generateResultSet(results, currentPage, pageSize) {
    if (!results || results.length === 0 || currentPage === 0) {
      return;
    }
    const startIndex = parseInt(pageSize * (currentPage - 1));
    const endIndex = startIndex + parseInt(pageSize);

    const pageResults = results.filter(
      (result) =>
        results.indexOf(result) >= startIndex &&
        results.indexOf(result) < endIndex
    );

    this.state.pageResults = pageResults;
  }

  render() {
    this.generateResultSet(
      this.props.results,
      this.props.currentPage,
      this.props.pageSize
    );

    return (
      <div>
        {this.state.pageResults.map((element) => (
          <Result
            key={this.state.pageResults.indexOf(element)}
            result={element}
            onDetailedPage={this.props.onDetailedPage}
          ></Result>
        ))}
      </div>
    );
  }
}
export default Results;
