import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import App from "./App";
const { faker } = require("@faker-js/faker");

beforeEach(() => {
  render(<App />);
});

test("when the page is loaded, the status text {0 Produkte verfügbar.} appears", () => {
  const statusText = "0 Produkte verfügbar.";
  expect(screen.getByText(statusText)).toBeInTheDocument();
});

// describe.skip("needs to shift / test with cypress", () => {
//   test("given search button, when click, then do not find any results", () => {
//     // given
//     const statusTextAfterButtonClick = "Keine Produkte gefunden";
//     const searchTerm = faker.random.word;
//     const statusText = "0 Produkte verfügbar.";

//     //console.log(screen.getByPlaceholderText(/Eingabe/i, searchTerm));
//     // when
//     userEvent.type(screen.getByPlaceholderText(/Eingabe/i, searchTerm));
//     userEvent.click(screen.getByText("Suchen"));

//     // then
//     return expect(screen.getByText(statusText)).resolves.toBe(
//       statusTextAfterButtonClick
//     );
//   });
// });
