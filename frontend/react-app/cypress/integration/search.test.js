describe("search find results", () => {
  beforeEach(() => {
    const url = Cypress.config() + "?data=" + Cypress.env("DATA");
    cy.visit(url);
    cy.findByRole("textbox").type("some text");
  });

  it("WHEN type searchtoken and click BUTTON, THEN n results are founnds", () => {
    cy.get("button").first().click();
    cy.get(".container")
      .eq(1)
      .find("div")
      .eq(0)
      .then(($elem) => {
        cy.log($elem.text());
      })
      .invoke("text")
      .should("match", /produkte gefunden/i)
      .and("not.match", /keine/i);
  });

  it("WHEN type searchtoken and click ENTER, THEN n results are founnds", () => {
    cy.findByRole("textbox").type("Cypress.io{enter}");
    cy.get(".container")
      .eq(1)
      .find("div")
      .eq(0)
      .then(($elem) => {
        cy.log($elem.text());
      })
      .invoke("text")
      .should("match", /produkte gefunden/i)
      .and("not.match", /keine/i);
  });
});

describe("search find no results", () => {
  beforeEach(() => {
    cy.visit(Cypress.config().baseUrl);
  });

  it("WHEN text is removed from text field, THEN button is disabled", () => {
    cy.findByRole("textbox").clear();
    cy.get("button")
      .first()
      .should("be.disabled")
      // let's enable this element from the test
      .invoke("prop", "disabled", false);
  });

  it("WHEN invalid search token, THEN no result text appears", () => {
    cy.findByRole("textbox").type("some text");
    // querySelector('#document-info')
    cy.get("button").first().click();

    cy.get(".container")
      .eq(1)
      .find("div")
      .then(($elem) => {
        cy.log($elem.text());
      })
      .invoke("text")
      .should("match", /Keine Produkte gefunden/i);
  });
});
