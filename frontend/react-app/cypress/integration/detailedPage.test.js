describe("jump to detailöed page", () => {
  beforeEach(() => {
    const url = Cypress.config().baseUrl + "?data=" + Cypress.env("DATA");
    cy.visit(url);

    cy.findByRole("textbox").type("some text");
    cy.get("button").first().click();

    // first search result
    cy.get(".container")
      .eq(1)
      .find("div")
      .eq(1)
      .find("div")
      .eq(0)
      .find("a")
      .eq(1)
      .click();
  });

  it("WHEN on DetailedPage, THEN table has theader and tbody", () => {
    // nutriTable

    cy.findByRole("table")
      .find("th")
      .should(($ths) => {
        expect($ths).to.have.length(1);
      });

    cy.findByRole("table")
      .find("tbody")
      .find("tr")
      .should(($trs) => {
        expect($trs.length).be.greaterThan(1);
      });
  });

  it("GIVEN on DetailedPage WHEN click on CLOSE button , THEN lightbox disappears again", () => {
    cy.get(".modal-footer")
      .find("button")
      .invoke("text")
      .should("match", /close/i);

    cy.get(".modal-footer").then(($el) => {
      Cypress.dom.isVisible($el); // true
    });
    cy.get(".modal-footer").should("exist");
    cy.get(".modal-footer").find("button").click({ force: true });
    cy.get(".modal-footer").should("not.exist");
  });
});
