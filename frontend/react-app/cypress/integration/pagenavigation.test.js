describe("search find results", () => {
  let firstSearchResult;
  let secondSearchResult;
  before(() => {
    const url = Cypress.config().baseUrl + "?data=" + Cypress.env("DATA");
    cy.visit(url);
    cy.findByRole("textbox").type("some text");
    cy.get("button").first().click();

    // search page 1
    cy.get("a")
      .eq(1)
      .invoke("text")
      .as("text")
      .then(($linkText) => {
        firstSearchResult = $linkText.trim();
      });

    // search page 2
    cy.get(".container").eq(1).get('[data-testid="buttonRight"]').click();

    cy.get("a")
      .eq(1)
      .invoke("text")
      .as("text")
      .then(($linkText) => {
        secondSearchResult = $linkText.trim();
      });

    cy.visit(url);
    cy.findByRole("textbox").type("some text");
    cy.get("button").first().click();
  });

  it("GIVEN first search result, WHEN click 2 time onto right nav button and 1 time on left nav button, THEN first page result differs", () => {
    cy.log(firstSearchResult);
    cy.log(secondSearchResult);

    cy.findByRole("link", {
      name: firstSearchResult,
    }).should("exist");

    cy.findByRole("link", {
      name: secondSearchResult,
    }).should("not.exist");

    // navigate to second page with indirection
    cy.get(".container").eq(1).get('[data-testid="buttonRight"]').click();
    cy.get(".container").eq(1).get('[data-testid="buttonRight"]').click();

    cy.get(".container").eq(1).get('[data-testid="buttonLeft"]').click();

    cy.findByRole("link", {
      name: firstSearchResult,
    }).should("not.exist");

    cy.findByRole("link", {
      name: secondSearchResult,
    }).should("exist");
  });
});
