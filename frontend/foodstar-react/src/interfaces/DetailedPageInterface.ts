type NutritionalValue = {
  energy: number;
  energy_kcal: number;
  fat: number;
  saturated_fat: number;
  carbs: number;
  sugar: number;
  protein: number;
  salt: number;
};

type ProductData = {
  productId: string;
  productName: string;
  nutritionalValue: NutritionalValue;
};

type DetailedPage = {
  productId: string;
  isModal: boolean;
  productData: ProductData;
  imageUrl: string;
};

export default interface DetailedPageInterface {
  detailedPage: DetailedPage;
}
