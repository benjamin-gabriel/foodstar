import * as React from "react";
import { Component } from "react";
import * as Globals from "./globals";
//import logo from "../images/logo.png";

type HeaderProps = {
  onSearchAction(
    searchTerm: string,
    e: React.KeyboardEvent<HTMLInputElement>
  ): void;
};

type HeaderState = {
  logoWidth: string;
  searchTerm: string;
  isButtonDisabled: boolean;
};

class Header extends React.Component<HeaderProps, HeaderState> {
  state = { logoWidth: "350px", searchTerm: "", isButtonDisabled: true };

  // ChangeEvent<MouseEvent>
  handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      searchTerm: e.target.value,
      isButtonDisabled: e.target.value !== "" ? false : true,
    });
  };

  render() {
    return (
      <header>
        <div className="container p-5" id="input">
          <div className="row justify-content-center p-5">
            <img
              className="header"
              src={null}
              style={{ width: this.state.logoWidth }}
              alt="Foodstar Logo"
            ></img>
          </div>

          <div className="row justify-content-center" id="input">
            <div className="col-sm-7 justify-content-end p-0">
              <input
                onChange={(e) => {
                  this.handleInputChange(e);
                }}
                onKeyDown={(e) =>
                  this.props.onSearchAction(this.state.searchTerm, e)
                }
                type="text"
                placeholder="Eingabe"
                name="producttoken"
                id="input-data"
                className="form-control form-control-lg"
                style={{ color: "green", fontSize: "1rem" }}
              ></input>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
