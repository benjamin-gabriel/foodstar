import * as React from "react";
import DetailedPageInterface from "../interfaces/DetailedPageInterface";
import { FC, ReactElement } from "react";
//import { Button, Modal } from "react-bootstrap";

const DetailedPage: FC<DetailedPageInterface> = (props) => {
  console.log("console.log test: ", props);
  return (
    <React.Fragment>
      <div>Product Id : {props.detailedPage.productId}</div>
      <div>In Detailed Page function....</div>
    </React.Fragment>
  );
};

export default DetailedPage;
