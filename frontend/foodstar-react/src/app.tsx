import * as React from "react";
import DetailedPage from "./components/detailedPage";
import Header from "./components/header";
import DetailedPageInterface from "./interfaces/DetailedPageInterface";
import * as Globals from "./components/globals";

const detailedPageData = {
  detailedPage: {
    isModal: true,
    productId: "123455",
    imageUrl: "imageurl.jpg",
    productData: {
      productId: "12345",
      productName: "my product",
      nutritionalValue: {
        carbs: 1,
        energy: 1,
        energy_kcal: 1,
        fat: 1,
        protein: 1,
        salt: 1,
        saturated_fat: 1,
        sugar: 1,
      },
    },
  },
} as DetailedPageInterface;

export default class App extends React.Component<{}> {
  state = {
    dataSourceType: Globals.DATASOURCE_WEBREQUEST,
    pageSize: 0,
    currentPage: 0,
    numberOfAllElements: 0,
    //loadedResults: null,
    apiUrl: "",
    detailedPage: detailedPageData,
  };

  searchHandler = async (
    searchTerm: string,
    e: React.KeyboardEvent<HTMLInputElement>
  ) => {
    if (e !== undefined && e.type === "keydown" && e.code !== "Enter") {
      return;
    }

    // const url = this.state.apiUrl + "/productlist?name=" + searchTerm;

    // const results = await this.getSearchResultData(
    //   url,
    //   Globals.PORDUCT_OVERVIEW_ITEM,
    //   DataSourceMock.TEST_FILE_MOCK_SEARCH_RESULT
    // );

    this.setState({
      loadedResults: null, // results
      currentPage: 1,
    });
  };

  render() {
    return (
      <React.Fragment>
        <div>App component</div>
        <Header onSearchAction={this.searchHandler} />
        <DetailedPage detailedPage={detailedPageData.detailedPage} />
      </React.Fragment>
    );
  }
}
