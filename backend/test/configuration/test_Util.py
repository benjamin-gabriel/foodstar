import unittest2
from src.configuration.Util import Util
from unittest.mock import patch, Mock
from src.service.ProductService import ProductService
import HtmlTestRunner


class UtilTest(unittest2.TestCase):
        def test_verify_and_normalize_str(self):
                input_no_str = object
                output_text = Util.validate_value_as_string('input', 'output')
                output_default_text = Util.validate_value_as_string(input_no_str, 'output')
                self.assertNotEqual(output_text, 'output', "output is not expected as second parameter.")
                self.assertEqual(output_default_text, 'output', "output is expected as second parameter.")

        def test_get_dictionary_value(self):
                dictionary = {}
                dictionary["key"] = "value"
                value = Util.dic_value(dictionary, "key")
                self.assertEqual(value, "value", "return value not as expected")
                self.assertNotEqual(value, "", "value is an empty string")

        def test_get_dictionary_2dim_value(self):
                dictionary = {}
                dictionary["key"] = {}
                dictionary["key"]["key2"] = "value"
                value = Util.dic_value(dictionary, "key", "key2")
                self.assertEqual(value, "value", "return value not as expected")
                self.assertNotEqual(value, "", "value is an empty string")


# python -m unittest discover
if __name__ == '__main__':
    import xmlrunner
    with open('results.xml', 'wb') as output:
        unittest2.main(
                testRunner=xmlrunner.XMLTestRunner(output=output),
                failfast=False,
                buffer=False,
                catchbreak=False)