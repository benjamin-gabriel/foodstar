import unittest2
from unittest.mock import patch, Mock
from src.model.DBColumnName import DBColumnName
from src.model.FoodItem import FoodDetailItem
from src.service.ImageService import ImageService
from src.configuration.Global import global_values
from src.model.DBMongoFieldName import DBMongoFieldName
import logging
import re
import HtmlTestRunner

# https://world.openfoodfacts.org/images/products/500/015/949/2539/front_de.13.400.jpg
class ProductServiceTest(unittest2.TestCase):

    def setUp(self):
        self.imageService = ImageService()
        
    def test_get_imgae_url_with_13_byte_product_id(self):
        productId = "1234567890123"
        self.imageService.set_product_id(productId)
        dic1 = {"rev": "REV"}
        url = self.imageService.get_image_url(dic1)
        part1 = productId[0:2]
        part2 = productId[3:5]
        part3 = productId[6:8]
        part4 = productId[9:12]
        x = re.search("^/[\d]{3}/[\d]{3}/[\d]{3}/[\d]{4}.*jpg$", url)
        self.assertTrue(x, "Wrong syntax in url")
        imagePathIncluded = (part1 in url) and (part2 in url) and (part3 in url)and (part4 in url)
        self.assertTrue(imagePathIncluded, "image not resolved correctly")

    def test_get_imgae_url_with_8_byte_product_id(self):
        productId = "12345678"
        self.imageService.set_product_id(productId)
        dic1 = {"rev": "REV"}
        url = self.imageService.get_image_url(dic1)
        #logging.warning('The url: ' + url)
        self.assertTrue((productId in url ), "Wrong URL syntax")


    def test_handle_imgae_url_with_invalid_byte_product_id(self):
        self.imageService.set_product_id("123")
        url = self.imageService.get_image_url({})
        self.assertEqual(url, "", "Invalid url should be an empty string.")

# python -m unittest discover
if __name__ == '__main__':
    import xmlrunner
    with open('results.xml', 'wb') as output:
        unittest2.main(
                testRunner=xmlrunner.XMLTestRunner(output=output),
                failfast=False,
                buffer=False,
                catchbreak=False)
    