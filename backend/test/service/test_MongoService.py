import unittest2
from unittest.mock import patch, Mock
from src.model.DBColumnName import DBColumnName
from src.model.FoodItem import FoodDetailItem
from src.service.ProductService import ProductService
from src.configuration.Global import global_values
from src.model.DBMongoFieldName import DBMongoFieldName
import HtmlTestRunner

#report: python -m xmlrunner discover -t . -o .

class ProductServiceTest(unittest2.TestCase):

    # validates service method get_product_by_id_method and the 
    # corresponding string in json synthax with realted and non related anttributes
    def test_get_product(self):
        productMock = {}
        productId = '6227607508b2e0e21e8c35e6'
        productMock['_id'] = productId
        productMock['code'] = '0000013000004'
        
        service = ProductService()
        with patch("src.repository.MongoRepository.MongoRepository.get_product_by_id") as get_product_by_id_method:
            get_product_by_id_method.return_value = productMock
            resulproductDetailObject = service.get_product(productId)
            self.assertIsInstance(resulproductDetailObject, str, "Result type is not as expected.")
            self.assertRegex(resulproductDetailObject, '.*energy.*', "Regex does not contain the expected attribute.")
            self.assertNotRegex(resulproductDetailObject, '.*calories.*', "Regex does not contain the expected attribute.")


    # validates get_foodproducts_by_name_method
    # 3 products return in an array list
    # second product has defined product name
    def test_get_products_by_name(self):
        productId = '6227607508b2e0e21e8c35e6'
        productName = 'Test Product'
        productListElements1 = {}
        productListElements2 = {}
        productListElements3 = {}
        productListElements2[DBMongoFieldName.code] = productId
        productListElements2[DBMongoFieldName.product_name] = productName
        productListMock = [productListElements1, productListElements2, productListElements3]
        numberOfProducts = len(productListMock)
        
        service = ProductService()
        with patch("src.repository.MongoRepository.MongoRepository.get_foodproducts_by_name") as get_foodproducts_by_name_method:
            get_foodproducts_by_name_method.return_value = productListMock
            with patch("src.repository.MongoRepository.MongoRepository.get_document_count") as get_document_count_method:
                get_document_count_method.return_value = numberOfProducts
                resultListAsJson = service.get_product_by_names(productName)
                self.assertIsInstance(resultListAsJson, str, "Result type is not as expected.")
                self.assertRegex(resultListAsJson, '.*product_name.*', "Regex does not contain the expected attribute.")
                self.assertRegex(resultListAsJson, '.*"product_id": "' + productId + '".*', "Regex does not contain the expected attribute.")
                self.assertRegex(resultListAsJson, '.*' + str(numberOfProducts) + '.*', "Regex does not contain the expected attribute.")


# python -m unittest discover
if __name__ == '__main__':
    import xmlrunner
    with open('results.xml', 'wb') as outputfile:
        unittest2.main(
                testRunner=xmlrunner.XMLTestRunner(output=outputfile),
                failfast=False,
                buffer=False,
                catchbreak=False)