from setuptools import setup, find_packages

setup(
    name="foodstar",
    version="0.4.1",
    author="Benjamin Gabriel",
    author_email="benjamin.gabriel@outlook.de",
    packages=find_packages(),
)