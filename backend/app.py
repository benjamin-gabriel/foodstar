from os.path import join, dirname
#from tty import CC
from dotenv import load_dotenv
from flask_cors import CORS
from flask import Flask
from src.controller.FoodController import FoodController
import mysql.connector
from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError
from flask import Response, request, jsonify

# init server objects
app = Flask(__name__)

api_v1_cors_config = {
    "origins": ["*"],
    "allow_headers": [
        "Authorization",
        "Content-Type",
        "Content-Search-Result",
        "Content-Page-Search",
    ],
    "expose_headers": ["content-search-next"],
}

CORS(app, resources={"/api/v1/*": api_v1_cors_config})

# define interfaces
app.add_url_rule(
    "/api/v1/productinfo", view_func=FoodController.product_info, methods=["GET"]
)
app.add_url_rule(
    "/api/v1/productlist", view_func=FoodController.product_list, methods=["GET"]
)
app.add_url_rule(
    "/api/v1/productcount", view_func=FoodController.product_count, methods=["GET"]
)

dotenv_path = join(dirname(__file__), "src/configuration/.env")
load_dotenv(dotenv_path)


# index route
@app.route("/")
def index():
    print("in index home route...")
    return "Status OK."


@app.route("/api/v1/headers")
def index2():
    resp = Response("Status OK, header test")
    resp.headers.add("key", "value")
    return resp


# validates connection to database
@app.route("/sqlconnect")
def sqlconnect():
    cnx = mysql.connector.connect(
        user="root",
        password="password",
        host="infrastructure-db-1",
        port="3306",
        database="food",
    )
    is_connected = cnx.is_connected()
    cnx.close()
    returnValue = "Connected to SQL database."
    if not (is_connected):
        returnValue = "Not connected to SQL database."
    return returnValue


# validates connection to database.
@app.route("/mongoconnect")
def mongoconnect():
    try:
        client = MongoClient("mongodb://admin:password@infrastructure-mongodb-1:27017/")
        return client.server_info()
    except ServerSelectionTimeoutError as err:
        client.close()
        return err
