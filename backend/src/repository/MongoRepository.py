from tokenize import Number
from pymongo import MongoClient
import os
from bson.objectid import ObjectId


class MongoRepository:
    def __init__(self):
        self.is_db_connected = False

    def __connectToDb(self):

        self.server = os.getenv("MONGO_HOST")
        self.port = os.getenv("MONGO_PORT")
        self.database = os.getenv("MONGO_DATABASE")
        self.username = os.getenv("MONGO_USERNAME")
        self.password = os.getenv("MONGO_PASSWORD")
        self.collection = os.getenv("MONGO_COLLECTION")

        try:
            self.client = MongoClient(
                "mongodb://"
                + self.username
                + ":"
                + self.password
                + "@"
                + self.server
                + ":"
                + self.port
            )
            self.is_db_connected = True
        except TypeError:
            print("Type error is raised")
            self.is_db_connected = False

    # defines the query and request do dbms with product id
    # returns result as json
    # example: {'_id': ObjectId('6227615b08b2e0e21e8c7770'), 'additives_tags': [], 'nutrition_data_per': '100g',
    def get_product_by_id(self, product_id):
        self.__connectToDb()
        db_collection = self.client.get_database(self.database).get_collection(
            self.collection
        )
        result = db_collection.find_one({"code": product_id})
        print("one result:")
        print(result)
        self.client.close()
        return result

    # defines the query and request do dbms with product name
    # returns result as n elements in list
    def get_foodproducts_by_name(self, product_name, page_number):
        result = []
        self.__connectToDb()
        db_collection = self.client.get_database(self.database).get_collection(
            self.collection
        )

        mongo_result = self.__execute_foodproducts_by_name(
            product_name, page_number, db_collection
        )
        for x in mongo_result:
            result.append(x)

        self.client.close()
        return result

    def __execute_foodproducts_by_name(self, product_name, page_number, db_collection):
        query = {"product_name": {"$regex": product_name, "$options": "i"}}
        #number_of_results = self.get_document_count(True, query)
        skip = 0
        limit = int(os.getenv("APP_SEARCH_PAGESIZE"))
        # if number_of_results + limit > page_number * limit:
        #     skip = (page_number - 1) * limit

        result = db_collection.find(query).skip(skip).limit(limit)
        return result

    def get_document_count(self, is_exact_query=False, query=""):
        if not self.is_db_connected:
            self.__connectToDb()

        db_collection = self.client.get_database(self.database).get_collection(
            self.collection
        )
        if is_exact_query:
            return db_collection.count_documents(query)
        else:
            return db_collection.estimated_document_count()


# d = {'a': 1, 'b': 2}
# %store d  # stores the variable
# del d

# %store -r d  # Refresh the variable from IPython's database.
# >>> d
# {'a': 1, 'b': 2}


# def __getSearchResultQueryByPageNumber(self, product_name, id_position):
#     query = ""
#     if id_position == "1" or len(id_position) != 24:
#         query = {"product_name": {"$regex": product_name, "$options": "i"}}
#     else:
#         query = {
#             "product_name": {"$regex": product_name, "$options": "i"},
#             "_id": {"$gt": ObjectId(id_position)},
#         }
#         # { _id: { $gt: ObjectId("5f7f1f7d67f67ddaa622b68e") } }
#         # db.users.find({ _id: { $gt: ObjectId("5f7f1f7d67f67ddaa622b68e") } }).limit(3).pretty()
#     return query
