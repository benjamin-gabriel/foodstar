from mysql.connector import connection
import os


class SqlRepository:
    def __init__(self):
        pass

    def __connectToDb(self):

        self.server = os.getenv("DB_HOST")  # os.getenv("DB_HOST")
        self.port = os.getenv("DB_PORT")  # os.getenv("DB_PORT")
        self.database = os.getenv("DB_DATABASE")  # os.getenv("DB_DATABASE")
        self.username = os.getenv("DB_USERNAME")  # os.getenv("DB_USERNAME")
        self.password = os.getenv("DB_PASSWORD")  # os.getenv("DB_PASSWORD")
        self.DRIVER = os.getenv("DB_DRIVER")  # os.getenv("DB_DRIVER")

        self.connection = connection.MySQLConnection(
            user=self.username,
            password=self.password,
            host=self.server,
            port=self.port,
            database=self.database,
            use_pure=True,
            get_warnings=True,
        )

    # defines the query and request do dbms with product id
    # returns result as 1 entry list
    def get_product_by_id(self, product_id):
        query = "SELECT * FROM export_german_result where code='" + product_id + "'"
        result = self.query(query)
        self.connection.close()
        return result

    # defines the query and request do dbms with product name
    # returns result as n elements in list
    def get_foodproducts_by_name(self, product_name, limit):
        results = []
        limit_statement = ""
        if limit.isdigit():
            limit_statement = "limit " + limit
        query = (
            "SELECT * FROM export_german_result where product_name LIKE '%"
            + product_name
            + "%' "
            + limit_statement
        )
        print(query)
        results = self.query(query)
        return results

    # execute sql query to the database
    def query(self, query):
        rows = []
        self.__connectToDb()
        cursor = self.connection.cursor()
        cursor.execute(query)
        row = cursor.fetchone()

        while row:
            rows.append(row)
            row = cursor.fetchone()

        return rows
