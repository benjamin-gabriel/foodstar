from src.model.MetaImageModel import MetaImageModel


# service to generate an image url from openfoodfacts repository
# image url example: https://world.openfoodfacts.org/images/products/500/015/949/2539/front_de.13.400.jpg
class ImageService:
    def __init__(self, base_url=""):
        self.base_url = base_url
        self.product_id = ""

    def set_product_id(self, product_id):
        self.product_id = product_id

    def get_image_url(
        self,
        dic_input,
        image_name=MetaImageModel.ImageType.front_de.value,
        image_viewport=MetaImageModel.ViewPort.small,
    ):
        url = self.base_url + self.__generate_image_url_path(
            image_name, dic_input, image_viewport
        )
        return url

    # Generates the external image url for 13 byte and 8 byte codes
    # example: 
    def __generate_image_url_path(self, image_name, dic_input, viewport):
        
        if len(self.product_id) != 8 and len(self.product_id) != 13:
            return ""
        
        try:
            path_product = ""
            if len(self.product_id) == 13:
                part1 = self.product_id[0:3]
                part2 = self.product_id[3:6]
                part3 = self.product_id[6:9]
                part4 = self.product_id[9:13]
                path_product = "/" + part1 + "/" + part2 + "/" + part3 + "/" + part4
            elif len(self.product_id) == 8:
                path_product = "/" + self.product_id

            path_image_meta = (
                "/" + image_name + "." + dic_input["rev"] + "." + viewport + ".jpg"
            )
            image_url = (path_product + path_image_meta).strip()
            return image_url
        except TypeError:
            print ("Log Type Error values in ImageService.__generate_image_url_path:")
            print ("image name: " + image_name)
            print ("viewport: " + viewport)
            return ""
