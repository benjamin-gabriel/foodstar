from src.repository.SqlRepository import SqlRepository
from src.model.FoodItem import FoodDetailItem, FoodOverviewItem
from src.model.DBColumnName import DBColumnName
from src.configuration.Util import Util


class SqlService:
    def __init__(self):
        self.food_repository = SqlRepository()

    # sort product by attribute
    def sort_products_by_productArribute(self, product_attribute):
        products = self.food_repository.get_product_by_name()
        products = sorted(products, key=lambda Product: Product.name)
        return products

    # fetch 1 food product from repository
    # return as dictionary speficic fields
    def get_product(self, id):
        foodproduct = self.food_repository.get_product_by_id(id)

        if len(foodproduct) == 1:

            productItem = FoodDetailItem(
                foodproduct[0][DBColumnName.code.value],
                foodproduct[0][DBColumnName.product_name.value],
                foodproduct[0][DBColumnName.energy_100g.value],
                foodproduct[0][DBColumnName.energy_kcal_100g.value],
                foodproduct[0][DBColumnName.fat_100g.value],
                foodproduct[0][DBColumnName.saturated_fat_100g.value],
                foodproduct[0][DBColumnName.carbohydrates_100g.value],
                foodproduct[0][DBColumnName.sugars_100g.value],
                foodproduct[0][DBColumnName.proteins_100g.value],
                foodproduct[0][DBColumnName.salt_100g.value],
            )

            return productItem
        else:
            return FoodDetailItem()

    # fetch 1 food product from repository (max 5)
    # return list with n products as dictionary elements
    def get_products_by_name(self, name, limit="0"):

        name = Util.validate_value_as_string(name)
        limit = Util.validate_value_as_string(limit, "NA")
        productList = []

        foodproducts = self.food_repository.get_foodproducts_by_name(name, limit)

        for foodproduct_index in range(0, len(foodproducts)):
            productItem = FoodOverviewItem(
                foodproducts[foodproduct_index][DBColumnName.code.value],
                foodproducts[foodproduct_index][DBColumnName.product_name.value],
                foodproducts[foodproduct_index][DBColumnName.image_small_url.value],
                foodproducts[foodproduct_index][DBColumnName.brand_owner.value],
            )
            productList.append(productItem)

        return productList
