from src.service.ImageService import ImageService
from src.model.FoodItem import FoodOverviewItem
from src.model.FoodItem import FoodDetailItem
from src.repository.MongoRepository import MongoRepository
from src.model.MetaImageModel import MetaImageModel
from src.configuration.Util import Util
from src.model.DBMongoFieldName import DBMongoFieldName
import logging

class MongoService:
    def __init__(self):
        self.mongo_repository = MongoRepository()
        self.image_service = ImageService(
            "https://world.openfoodfacts.org/images/products"
        )

    # todo write service
    def __get_kcal_by_kj(self, value_kj):
        try:
            value_kj_float = float(value_kj)
            value_kcal_int = int(value_kj_float * 0.24)
            return str(value_kcal_int)
        except:
            return "0"

    # remark: image dictionary is subset of product dictionary
    def __get_image_url(self, image_dictionary):
        if (not isinstance(image_dictionary, dict) or DBMongoFieldName.images not in image_dictionary):
            return ""
        image_type = ""
        if (MetaImageModel.ImageType.incredient_de.value in image_dictionary):
            image_type = MetaImageModel.ImageType.incredient_de.value
        elif(MetaImageModel.ImageType.front_de.value in image_dictionary):
            image_type = MetaImageModel.ImageType.front_de.value
        image_url = self.image_service.get_image_url(Util.dic_value(image_dictionary, image_type), image_type, MetaImageModel.ViewPort.medium)
        return image_url

    # fetch 1 food product from repository
    # return as dictionary speficic fields
    def get_product(self, id):
        result = self.mongo_repository.get_product_by_id(id)
        self.image_service.set_product_id(Util.dic_value(result, DBMongoFieldName.code))

        image_name = ""
        image_url = "images/default_food.png"  # todo default image url from .env
        if (DBMongoFieldName.images in result and 
            Util.get_validated_dic_value(
                Util.dic_value(result, DBMongoFieldName.images), MetaImageModel.ImageType.incredient_de.value
            )
            != ""
        ):
            image_name = MetaImageModel.ImageType.incredient_de.value
            image_url = self.image_service.get_image_url(
                Util.dic_value(result, DBMongoFieldName.images, image_name), image_name, MetaImageModel.ViewPort.medium
            )
        elif ("images" in result and 
            Util.get_validated_dic_value(
                Util.dic_value(result, DBMongoFieldName.images), MetaImageModel.ImageType.front_de.value
            )
            != ""
        ):
            image_name = MetaImageModel.ImageType.front_de.value
            image_url = self.image_service.get_image_url(
                Util.dic_value(result, DBMongoFieldName.images, image_name), image_name, MetaImageModel.ViewPort.medium
            )

        image_url = self.__get_image_url(result)

        # logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
        # logging.debug('This message should appear on the console')
        # logging.info('So should this')
        # logging.warning('And this, too')

        productItem = FoodDetailItem(
            Util.dic_value(result, DBMongoFieldName.code),
            Util.dic_value(result, DBMongoFieldName.product_name),
            Util.dic_value(result, DBMongoFieldName.nutriments, DBMongoFieldName.Nutriment.energy_100g),
            self.__get_kcal_by_kj(Util.dic_value(result, DBMongoFieldName.nutriments, DBMongoFieldName.Nutriment.energy_100g)),
            Util.dic_value(result, DBMongoFieldName.nutriments, DBMongoFieldName.Nutriment.fat_100g),
            Util.dic_value(result, DBMongoFieldName.nutriments, DBMongoFieldName.Nutriment.saturated_fat_100g),
            Util.dic_value(result, DBMongoFieldName.nutriments, DBMongoFieldName.Nutriment.carbohydrates_100g),
            Util.dic_value(result, DBMongoFieldName.nutriments, DBMongoFieldName.Nutriment.sugars_100g),
            Util.dic_value(result, DBMongoFieldName.nutriments, DBMongoFieldName.Nutriment.proteins_100g),
            Util.dic_value(result, DBMongoFieldName.nutriments, DBMongoFieldName.Nutriment.salt_100g),
            image_url,
        )

        return productItem



    # fetch 1 food product from repository (max 5)
    # return list with n products as dictionary elements
    def get_products_by_name(self, name, page_number):

        if page_number < 1:
            page_number = 1
        resultAsDicInList = self.mongo_repository.get_foodproducts_by_name(
            name, page_number
        )
        productList = []
        image_url = ""

        for element in resultAsDicInList:
            if (
                "images" in element
                and MetaImageModel.ImageType.front_de.value in element["images"]
            ):
                self.image_service.set_product_id(element["code"])
                image_url = self.image_service.get_image_url(
                    element["images"][
                        MetaImageModel.ImageType.front_de.value
                    ],  # todo exception handling
                    MetaImageModel.ImageType.front_de.value,
                )

            productItem = FoodOverviewItem(
                Util.dic_value(element, DBMongoFieldName.code),
                Util.dic_value(element, DBMongoFieldName.product_name),
                Util.dic_value(element, DBMongoFieldName.generic_name),
                image_url,
            )
            productList.append(productItem)
        return productList

    def get_product_count(self):
        result = self.mongo_repository.get_document_count()
        return str(result)
