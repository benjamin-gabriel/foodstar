from src.repository.MongoRepository import MongoRepository
from src.service.MongoService import MongoService
from src.model.FoodItem import FoodItem
from src.configuration.Util import Util
from src.model.DBType import DBType
from src.service.SqlService import SqlService
from src.model.FoodSearchResultItem import FoodSearchResultItem


class ProductService:
    def __init__(self):
        self.mongo_service = MongoService()
        self.mongo_repository = MongoRepository()
        self.sql_service = SqlService()
        self.db_type = DBType.mongodb

    # sort product by attribute
    # def sort_products_by_productArribute(self, product_attribute):
    #   products = self.food_repository.get_product_by_name()
    #    products = sorted(products, key=lambda Product: Product.name)
    #    return products

    # get data from db service
    def get_product(self, id):

        productItem = {}

        if self.db_type == DBType.mysql:
            productItem = self.sql_service.get_product(id)

        elif self.db_type == DBType.mongodb:
            productItem = self.mongo_service.get_product(id)

        productItemEncoded = Util.FoodEncoder().encode(productItem)
        return productItemEncoded

    # get data from db service and prepeare search result item
    def get_product_by_names(self, name, page_number=1):

        productList = []

        if self.db_type == DBType.mysql:
            productList = self.sql_service.get_products_by_name(name)

        elif self.db_type == DBType.mongodb:
            mongoService = MongoService()
            productList = mongoService.get_products_by_name(name, page_number)

        searchResultItem = self.__generate_no_results_found_message_for_empty_list(
            productList,
            self.mongo_repository.get_document_count(
                True, {"product_name": {"$regex": name, "$options": "i"}}
            ),
        )
        searchResultItemEncoded = Util.FoodEncoder().encode(searchResultItem)
        return searchResultItemEncoded

    def get_product_count(self):
        if self.db_type == DBType.mongodb:
            return self.mongo_service.get_product_count()

    def __generate_no_results_found_message_for_empty_list(
        self, input, element_count=0
    ):
        if element_count == 0:
            return FoodSearchResultItem("no prodcuts are found.")
        else:
            return FoodSearchResultItem(
                input, str(element_count) + " products are found.", element_count
            )
