from enum import Enum


class ProductAttribute(Enum):
    NAME = 1
    CALORIES = 2
    SUGAR = 3
