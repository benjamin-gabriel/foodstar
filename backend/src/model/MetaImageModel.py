from enum import Enum


class MetaImageModel:
    class ViewPort:
        small = "100"
        medium = "200"
        large = "400"
        full = "full"

    class ImageType(Enum):
        front_de = "front_de"
        incredient_de = "incredient_de"
