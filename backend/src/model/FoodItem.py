class FoodItem:
    def __init__(self, product_id, product_name):
        self.product_id = product_id
        self.product_name = product_name


# data object for the product detail page
class FoodDetailItem(FoodItem):
    def __init__(
        self,
        product_id,
        product_name,
        energy,
        energy_kcal,
        fat,
        saturated_fat,
        carbs,
        sugar,
        protein,
        salt,
        image_url,
    ):
        # composition classes
        class NutritionalValue:
            def __init__(
                self,
                energy,
                energy_kcal,
                fat,
                saturated_fat,
                carbs,
                sugar,
                protein,
                salt,
            ):
                self.energy = energy
                self.energy_kcal = energy_kcal
                self.fat = fat
                self.saturated_fat = saturated_fat
                self.carbs = carbs
                self.sugar = sugar
                self.protein = protein
                self.salt = salt

        # parent
        super().__init__(product_id, product_name)

        # composition
        self.nutritional_value = NutritionalValue(
            energy, energy_kcal, fat, saturated_fat, carbs, sugar, protein, salt
        )

        # self
        self.image_url = image_url


# data object for search results
class FoodOverviewItem(FoodItem):
    def __init__(self, product_id, product_name, product_description, thumbnail_url):
        super().__init__(
            product_id,
            product_name,
        )
        self.thumbnail_url = thumbnail_url
        self.product_description = product_description
