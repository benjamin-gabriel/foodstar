from enum import Enum


class DBType(Enum):
    mysql = 0
    mongodb = 1
