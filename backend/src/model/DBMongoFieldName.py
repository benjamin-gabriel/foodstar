class DBMongoFieldName:
    code = "code"
    images = "images"
    image_name = "image_name"
    product_name = "product_name"
    nutriments = "nutriments"
    images = "images"
    generic_name = "generic_name"

    class Nutriment:
        energy_100g = "energy_100g"
        fat_100g = "fat_100g"
        saturated_fat_100g = "saturated-fat_100g"
        carbohydrates_100g = "carbohydrates_100g"
        sugars_100g = "sugars_100g"
        proteins_100g = "proteins_100g"
        salt_100g = "salt_100g"