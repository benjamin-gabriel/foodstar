from decimal import Decimal
from json import JSONEncoder


class Util:
    def __init__(self):
        pass

    class FoodEncoder(JSONEncoder):
        def default(self, o):
            self.item_separator
            if type(o) is not Decimal:
                return o.__dict__
            else:
                print(o)
                return ""

    @staticmethod
    def validate_value_as_string(value, default_string=""):
        if type(value) is str:
            return value
        else:
            return default_string

    @staticmethod
    def get_validated_dic_value(dic_to_validate, key):
        if key in dic_to_validate:
            return dic_to_validate[key]
        else:
            return ""


    @staticmethod
    def dic_value(dic, key, key_2_dim=""):
        if not isinstance(dic, dict):
            return ""
        elif not key in dic:
            return ""
        elif key_2_dim == "" and key in dic: # 1 dim value
            return dic[key]
        elif key_2_dim != "" and key in dic and key_2_dim in dic[key]: # 2 dim value
            return dic[key][key_2_dim]
        else:
            return ""
