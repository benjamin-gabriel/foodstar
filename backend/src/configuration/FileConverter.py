import bson
from bson import json_util
import json


class FileConverter:
    def __init__(self, filename):
        self.filename = filename

    def generate_and_replace_mongo_object_id(self):

        file = open(self.filename, encoding="utf8")
        lines = file.readlines()
        new_lines = []
        counter = 0
        for line in lines:
            json_object = json.loads(line)
            if len(json_object["_id"]) < 24:
                new_id = str(bson.objectid.ObjectId())

                json_object["_id"] = new_id
                json_object_normlized = json.loads(json_util.dumps(json_object))
                json_object_text = json.dumps(json_object_normlized)
                print(str(counter) + " ")
                counter += 1
                new_lines.append(json_object_text)

        with open("c:/temp/outfile", "w") as outfile:
            outfile.write("\n".join(new_lines))
