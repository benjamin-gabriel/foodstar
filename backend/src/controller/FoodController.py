from asyncio import exceptions
from src.service.ProductService import ProductService
from flask import Response, request, jsonify
from flask import request
import json


class FoodController:

    # extract the id and delegates to the controller
    # returns 1 food product result in json text format
    def product_info():
        id = request.args.get("id")
        try:
            if type(id) is not str:
                raise exceptions.InvalidStateError("id parameter is mandatory.")
        except exceptions.InvalidStateError as e:
            return Response(
                "{'error': '" + str(e) + "'}", status=400, mimetype="application/json"
            )
        else:
            product_service = ProductService()
            result = product_service.get_product(id)
            resultAsJson = jsonify(result)
            return resultAsJson

    # extract the name and result limit, delegates to the controller
    # returns result of n food products in json text format
    @staticmethod
    def product_list():
        name = request.args.get("name")
        page = "1" #request.args.get("page")

        try:
            assert isinstance(name, str)
            #assert isinstance(page, str) and page.isdigit
            #assert "Content-Page-Search" in request.headers
        except AssertionError as e:
            return Response(
                "{'error': '" + str(e) + "'}", status=400, mimetype="application/json"
            ).headers.add("Content-Search-Result", False)

        page_number = int(page)
        product_service = ProductService()
        result = product_service.get_product_by_names(name, page_number)
        resultAsJson = Response(json.dumps(result), mimetype="application/json")
        resultAsJson.headers.add("Content-Search-Result", True)
        return resultAsJson

    @staticmethod
    def product_count():
        product_service = ProductService()
        # result = product_service.get_product_count()
        # resultAsJson = Response(json.dumps(result), mimetype="application/json")
        numberOfDocuments = product_service.get_product_count()
        resultAsJson = Response(
            json.dumps('{"product_count":"' + numberOfDocuments + '"}'),
            mimetype="application/json",
        )

        return resultAsJson
