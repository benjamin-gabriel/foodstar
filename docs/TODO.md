## Infrastructure

## required
- [ ] fill mongo import when container is starting

## optional
- [ ] Use Docker code for VS-Code Editor
- [x] Docker-Compose service internal port defintions
- [ ] write Postman Integration test cases
- [ ] image service unit test
- [ ] no result message