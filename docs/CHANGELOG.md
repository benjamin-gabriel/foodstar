#### 0.4.0 (April 1, 2022)

##### Changes


* **Fixed:**
  *  added package.json project description for npm exec again ([39c53687]
  *  detailed page json structure ([e3276230]
  *  adapt status message logic ([52c71dac]
  *  adapt search logic backend, skip broken images ([cacdad33]
  *  added package.json project description for npm exec again ([3df16114]
  *  detailed page json structure ([26c42a64]
  *  adapt search logic backend, skip broken images ([5d30b8ca]
  *  read .env variables consistently ([f5aefb8c]
  *  retain docker-compose conistent database and collection ([a9ff3d6b]
  *  adapt status message logic ([f9e10161]
* **Added:**
  *  Detail Page unit test implementation ([6778a171]
  *  frontent react-app <header unit test implementation ([423b816f]
  *  react-app unit test implementation paging, mainarea ([d774d672]
  *  stub data integration, unit tests header area integration ([9d51dd87]
  *  detail page lightbox modal implementation ([7a56af14]
  *  FE detail page integration ([871d0d5b]
  *  .env logic implementation ([18f13661]
  *  fe implementation paging ([d0e7f2bb]
  *  create reacta-pp fe, implement components searchpage ([847ef7fc]
  *  frontent react-app <header unit test implementation ([f7ff3f1a]
  *  react-app unit test implementation paging, mainarea ([65927621]
  *  stub data integration, unit tests header area integration ([5ed343ad]
  *  .env logic implementation ([b0a98d8e]
  *  fe implementation paging ([158a14c3]
  *  Frontend integration paging ([1120a774]
  *  document count implementation ([8c5ca151]
  *  search result paging ([6db07cec]
  *  script scope for file handling ([ca8b88a8]
  *  Detail Page unit test implementation ([4ddf015c]
  *  detail page lightbox modal implementation ([9d0d5fee]
  *  FE detail page integration ([a19e1558]
* **Changed:**
  *  adapt docker-compose structure and naming ([afbd6ee0]
  *  Restructure infrasturcure logic ([22231fbe]
  *  Style improvements ([77e02aa0]
  *  Restructure infrasturcure logic ([b447adf2]
  *  Style improvements ([cdd614a4]
  *  search result paging implementation, considered invalid page values ([83496ce5]
  *  paging via id - backend ([26201b6c]
  *  extend number of documents presentation ([6fa95fb9]
  *  generate detailpage in index.html ([d80a4c9d]
  *  pagesize and axios integration into react-app ([9bbfd9c4]
  *  adapt docker-compose structure and naming ([367787dc]
  *  pagesize and axios integration into react-app ([da4871bd]


#### Release v0.3.0 (March 6, 2022)

##### Changes

* **Fixed:**
  *  adapt image url generation ([bf946ec2](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/bf946ec2c7ef255f2900e9acb9e94d5e869cb2d1))
  *  refactored result search item with message structure ([a04e34a0](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/a04e34a01b9b3748b7dc7285f206da42c14dbf3b))
  *  Adapt code refactoring settings ([0db43ebd](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/0db43ebdf39b8f4e5725f501364b3cbfacae85a1))
  *  Adapt code refactoring settings ([457eb084](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/457eb084acf4f148271fb4f48f881198c6d97c0a))
* **Changed:**  outsource sql logic from Product service ([d698291d](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/d698291dd82e942823513eb8423ec449b7efb2ab))
* **Added:**
  *  added changelog formatter generate-changelog ([c2bec9f2](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/c2bec9f22380f9b5b49dc72171bff6e63cbd1e1d))
  *  field message for product data objects ([cade8e71](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/cade8e71b4853b222035e86ea4930710bbba0a42))
  *  bootstrap integration overview page ([97e18a71](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/97e18a71e103d8440214296074b848be6334cf4d))
  *  Image service implementation to handle images from MongoDB to Frontend ([3c1d61b3](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/3c1d61b3637199b40acf1ca56a6c73ec0bf52fe0))
  *  MongoDB service implementation ([38952835](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/3895283583424a10cc367671ae5386fa202dccc1))
  *  MongoDB infrastructure implementation ([854d5060](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/854d5060a733a7c0e8f1e8e7cfc2a45c1ed0861a))
  *  docker-compose mysql service, start/stop sh script ([588f255b](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/588f255b0502d4346103cdcb93e8ae0f98b84d39))
  *  Docker Infrastructure for python, mongo db ([0bff4e22](git+ssh://git@gitlab.com/benjamin-gabriel/foodbackend/commit/0bff4e22ab70919e5d72eefa9f1cb9939abfbdc2))

#### Release v0.2.0 - (February 22, 2022)
- fe detail page implementation
- fix namespace
- re rename controller in Upper letter
- rename controller
- refactoring coding guideline
- food data object implementation
- fe search by name, fix response json
- unit test implementation for service
- venv setup, launch.json for debugging
- merge db and repository class
- mergin from development branch, resolve merge conflicts
- Merge branch 'development' into be
- fix namespace
- re rename controller in Upper letter
- rename controller
- refactoring coding guideline
- food data object implementation
- fe search by name, fix response json
- unit test implementation for service
- venv setup, launch.json for debugging
- merge db and repository class
- mergin from development branch, resolve merge conflicts
- Merge branch 'development' into be
- refactor food controller to route API calls      
- unit test implementation
- adpat parameter values and fix services with these parameterse parameters    

#### Release v0.1.0 - (February 13, 2022)
- integrate data object, web interface by name impl, extend debug file
- env file implementation
- .env file implementation
- FE implementation crosscut to BE , add core support
- reorganize project, fe - be separation
- webserver initialisation, get product by id web request implementation
- init project, repository to read data from db implementation
- integrate data object, web interface by name impl, extend debug file
- env file implementation
- .env file implementation
- FE implementation crosscut to BE , add core support
- reorganize project, fe - be separation
- webserver initialisation, get product by id web request implementation
- init project, repository to read data from db implementation
- Initial commit